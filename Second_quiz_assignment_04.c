#include <stdio.h>
#include <stdlib.h>

int main()
{
    float r,h,v;
    float pi=3.14;
    printf("\nEnter the radius: ");
    scanf("%f",&r);
    printf("\nEnter the height: ");
    scanf ("%f",&h);
    v=(pi*r*r*h)/3.0;
    printf("Volume of the cone: %.2f",v);
    return 0;
}
